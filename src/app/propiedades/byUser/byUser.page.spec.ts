import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ByUserPage } from './byUser.page';

describe('AgregarPage', () => {
  let component: ByUserPage;
  let fixture: ComponentFixture<ByUserPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ByUserPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ByUserPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
