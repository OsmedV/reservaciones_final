import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";

import { LoginComponent } from "./login/login.component";
import { UserComponent } from "./user/user.component";
import { RegisterComponent } from "./register/register.component";
import { UserResolver } from "./user/user.resolver";
import { AuthGuard } from "./core/auth.guard";


const routes: Routes = [
  {
    path: "home",
    loadChildren: () =>
      import("./home/home.module").then((m) => m.HomePageModule),
  },
  {
    path: "",
    redirectTo: "home",
    pathMatch: "full",
  },
  { path: "login", component: LoginComponent, canActivate: [AuthGuard] },
  { path: "register", component: RegisterComponent, canActivate: [AuthGuard] },
  { path: "user", component: UserComponent, resolve: { data: UserResolver } },
  {
    path: "propiedades",
    children: [
      {
        path: "",
        loadChildren: () =>
          import("./propiedades/propiedades.module").then(
            (m) => m.PropiedadesPageModule
          ),
      },
      {
        path: "agregar",
        loadChildren: () =>
          import("./propiedades/agregar/agregar.module").then(
            (m) => m.AgregarPageModule
          ),
      },
    ],
  },
  {
    path: 'reservar',
    loadChildren: () => import('./propiedades/reservar/reservar.module').then( m => m.ReservarPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
